from IPython import display
import glob
import imageio
import matplotlib.pyplot as plt
import numpy as np
import PIL
import tensorflow as tf
import tensorflow_probability as tfp
import time
import os
import PIL
import PIL.Image
import pathlib

data_dir = pathlib.Path('D:/rendered_chairs')
image_count = len(list(data_dir.glob('*/*/*.png')))
print(image_count)

image_list = list(data_dir.glob('*/renders/image_025_*.png'))
print(len(image_list))
image_count = len(image_list)

batch_size = 32
img_height = 600
img_width = 600
img_channels = 3
test_fraction = 0.2

image_ds = tf.data.Dataset.list_files('D:/rendered_chairs/*/renders/image_025_*.png', shuffle = False)
image_ds = image_ds.shuffle(image_count)

# for f in image_ds.take(5):
#     print(f.numpy())


# PIL.Image.open(str(image_list[0])).show()

test_size = int(image_count * test_fraction)
train_size = image_count - test_size
train_ds = image_ds.skip(test_size)
test_ds = image_ds.take(test_size)

def decode_img(img):
    img = tf.io.decode_png(img, channels=3)
    return tf.image.resize(img, [img_height, img_width])/255.

def process_path(file_path):
    img = tf.io.read_file(file_path)
    img = decode_img(img)
    return img

def parse_image(file_path):
    image = tf.io.read_file(file_path)
    image = tf.image.decode_png(image, channels=3)
    image = tf.image.convert_image_dtype(image, tf.float32)
    image = tf.image.resize(image, [img_height, img_width])/255.
    return image

train_ds = train_ds.map(parse_image)
test_ds = test_ds.map(parse_image)

train_dataset = train_ds.shuffle(train_size).batch(batch_size)
test_dataset = test_ds.shuffle(test_size).batch(batch_size)
print(test_dataset)


class CVAE(tf.keras.Model):
    def __init__(self, latent_dim):
        super(CVAE, self).__init__()
        self.latent_dim = latent_dim
        self.encoder = tf.keras.Sequential(
            [
                tf.keras.layers.InputLayer(input_shape=(600, 600, 3)),
                tf.keras.layers.Conv2D(
                    filters=32, kernel_size=10, strides=(2,2), activation='relu'),
                tf.keras.layers.Conv2D(
                    filters=64, kernel_size=5, strides=(2,2), activation='relu'),
                tf.keras.layers.Conv2D(
                    filters=128, kernel_size=3, strides=(2,2), activation='relu'),
                tf.keras.Flatten(),
                tf.keras.Dense(latent_dim + latent_dim),
            ]
        )

        self.decoder = tf.keras.Sequential(
            [
                tf.keras.layers.InputLayer(input_shape=(latent_dim,)),
                tf.keras.layers.Dense(units = 10*10*32),
                tf.keras.layers.Reshape(target_shape=(10,10,32)),
                tf.keras.layers.Conv2DTranspose(
                    filters=128, kernel_size=3, strides=2, padding='same',
                    activation='relu'),
                tf.keras.layers.Conv2DTranspose(
                    filters=64, kernel_size=5, strides=2, padding='same',
                    activation='relu'),
                tf.keras.layers.Conv2DTranspose(
                    filters=32, kernel_size=10, strides=2, padding='same',
                    activation='relu'),
                tf.keras.layers.Conv2DTranspose(
                    filters=3, kernel_size=3, strides=2, padding='same'),
                
            ]    
        )

    @tf.function
    def sample(self, eps=None):
        if eps is None:
            eps = tf.random.normal(shape=(100, self.latent_dim))
        return self.decode(eps, apply_sigmoid=True)

    def encode(self, x):
        mean, logvar = tf.split(self.encoder(x), num_or_size_splits=2, axis=1)
        return mean, logvar

    def reparametrize(self, mean, logvar):
        eps = tf.random.normal(shape=mean.shape)
        return eps * tf.exp(logvar*0.5) + mean

    def decode(self, z, apply_sigmoid=False):
        logits = self.decoder(z)
        if apply_sigmoid:
            probs = tf.sigmoid(logits)
            return probs
        return logits

optimizer = tf.keras.optimizers.Adam(1e-4)
def log_normal_pdf(sample, mean, logvar, raxis=1):
    print('ciao')